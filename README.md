# k8s-utils

## Why
1. Often in a CI or CD pipeline you need to utilise kubectl, helm or other k8s related utils in a job. Often there is a lack of official images for these simple utilities which means it takes time to find, test and maintain various images to utilise in the pipeline.
1. Rolling an installation as part of a job is not difficult but can lengthen the time of the job and thus the entire pipeline. A few seconds here and there doesn't mattter but when it adds up to minutes in a rapidly iterating branch, time is money!
1. There are seperate maintained images from the likes of bitnami and others but often they are so stripped or locked down that there is not even a usable shell.

## What
A simple, small, Alpine image with the following installed (open to suggestions for other utils).
* kubectl (reasonably recent)
* helm  (version 3)
* A usable shell

## Pull the image
```bash
docker pull registry.gitlab.com/seanel/k8s-utils:latest
```

## Testing
* The image is scanned using the excellent https://github.com/aquasecurity/trivy for vulnerable OS or installed packages
* Tests of the utils (simple <tool-name --version> typically)
* An automated pipeline updates, builds and re-tests on a fortnightly basis
* Thats it!
